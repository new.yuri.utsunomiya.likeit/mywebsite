package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.User;
import dao.TodoDao;
import dao.UserDao;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    
//idを取得
    int id = user.getId();
    
    //リレーション先のデータを先に削除
    TodoDao todoDao = new TodoDao();
    todoDao.deleteUser(id);
    
    
    // emailをキーにパラメータを取得
//    String email = request.getParameter("email");
    String email   = user.getEmail();
    // emailを引数にメソッドを呼び出し、単純に削除のみ（確認画面なし）
    UserDao userDao = new UserDao();
    userDao.deleteUser(email);


    response.sendRedirect("LoginServlet");
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    doGet(request, response);
  }

}

package servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Todo;
import beans.User;
import dao.TodoDao;

/**
 * Servlet implementation class SearchServlet
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

   // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/search.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    // ログインチェック
	    HttpSession session = request.getSession();
	    User user = (User) session.getAttribute("userInfo");
	    
	    // 文字コードを設定
	    request.setCharacterEncoding("UTF-8");
	    // 記入・送信された内容が格納されているリクエストパラメータを取得
	    String title = request.getParameter("title");
	    int id = user.getId();

	    // 検索を実行するメソッド
	    TodoDao todoDao = new TodoDao();
	    List<Todo> todoSearchList = todoDao.search(title, id);

	    // 戻り値をリクエストスコープへセット
	    request.setAttribute("todoSearchList", todoSearchList);
	 // フォワード
	    RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/search.jsp");
	    dispatcher.forward(request, response);

	  
	}

}

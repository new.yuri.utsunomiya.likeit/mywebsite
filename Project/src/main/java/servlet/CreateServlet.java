package servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Todo;
import beans.User;
import dao.TodoDao;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CreateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // 文字コードを設定
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      User user = (User) session.getAttribute("userInfo");

      // TODOIDとユーザーIDを取得
      Todo todo = new Todo();
      int id = todo.getId();
      int userId = user.getId();

      // 記入され送信されたリクエストパラメータを取得
      String title = request.getParameter("title");

      boolean isEmpty = title.equals("");

      if (isEmpty) {
        request.setAttribute("errMsg", "TODOを入力してください");

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
        dispatcher.forward(request, response);
        return;
      }
      // DB書き込み
      TodoDao todoDao = new TodoDao();
      TodoDao.addTodo(id, title, userId);
      request.setAttribute(title, todoDao);

      response.sendRedirect("ListServlet");

    } catch (IllegalArgumentException e) {
      e.printStackTrace();


    }
  }

}

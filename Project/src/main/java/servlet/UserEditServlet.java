package servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserEditServlet
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      
	    HttpSession session = request.getSession();
	    User user = (User) session.getAttribute("userInfo");
	  

      int id   = user.getId();
//    String userName = user.getName();
//    String email = user.getEmail();
//    String password = user.getPassword();
      
      UserDao userDao = new UserDao();
      user = userDao.findUserById(id);

      request.setAttribute("userInfo", user);
      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userEdit.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  // リクエストパラメータの文字コードを指定
	    request.setCharacterEncoding("utf-8");
	    
	    HttpSession session = request.getSession();

	    try {
	      // 記入され送信されたリクエストパラメータを取得
	      
	      String strId = request.getParameter("id");
	      String inputUserName = request.getParameter("userName");
	      String inputEmail = request.getParameter("email");
	      String inputPassword = request.getParameter("password");

//	      User user = new User();
//	      int id = user.getId();
//
	      
	      
	      boolean nameIsEmpty = inputUserName.equals("");
	      boolean emailIsEmpty = inputEmail.equals("");
	      boolean passwordIsEmpty = inputPassword.equals("");
	      
	      if(nameIsEmpty || emailIsEmpty || passwordIsEmpty) {
	        
	        int id = Integer.valueOf(strId);
	        
	         UserDao userDao = new UserDao();
	          User userEdit = userDao.findUserById(id);
	          
	          userEdit.setName(inputUserName); //ここが問題
	          userEdit.setEmail(inputEmail);
	          userEdit.setPassword(inputPassword);
	        
	        request.setAttribute("userInfo", userEdit);
	        
	     // リクエストスコープにエラーメッセージをセット→setAttribute
	        request.setAttribute("errMsg", "空欄では登録できません");
	        
	        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userEdit.jsp");
	        dispatcher.forward(request, response);
	        return;
	        
	      }
	      UserDao userDao = new UserDao();
	      int id = Integer.parseInt(strId);
//	      User userEdit = userDao.findUserById(id);
	      
	      
	      User user = new User();
	      user.setId(id); //ここが問題
          user.setName(inputUserName); //ここが問題
          user.setEmail(inputEmail);
          user.setPassword(inputPassword);
	      
	      //DBに上書きする
	      userDao.updateUser(inputUserName, inputEmail, inputPassword, id);
	      
	      
	   // 登録が確定されたかどうか確認するための変数
	      String confirmed = request.getParameter("confirm_button");

	      switch (confirmed) {
	      case "edit":
	          session.setAttribute("userInfo", user);
	          response.sendRedirect("ListServlet");
	          break;
	      }
	      

//	      response.sendRedirect("ListServlet");
//	      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/ListServlet");
//	      dispatcher.forward(request, response);

	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}

}

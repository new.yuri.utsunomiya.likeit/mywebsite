package servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Todo;
import beans.User;
import dao.TodoDao;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public ListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {


    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    int id = user.getId();
    TodoDao todoDao = new TodoDao();
    List<Todo> todoList = todoDao.findAll(id);

    // 戻り値をリクエストスコープにセット
    request.setAttribute("todoList", todoList);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
    dispatcher.forward(request, response);


  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    
 
    
    
  }

}

package servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class SignupConfirmServlet
 */
@WebServlet("/SignupConfirmServlet")
public class SignupConfirmServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public SignupConfirmServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("utf-8");

    try {
      // 記入され送信されたリクエストパラメータを取得
      String inputUserName = request.getParameter("userName");
      String inputEmail = request.getParameter("email");
      String inputPassword = PasswordEncoder.encodePassword(request.getParameter("password"));

      User user = new User();
      user.setName(inputUserName);
      user.setEmail(inputEmail);
      user.setPassword(inputPassword);

      request.setAttribute("user", user);
      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/signupConfirm.jsp");
      dispatcher.forward(request, response);

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}

package servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Todo;
import beans.User;
import dao.TodoDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // idをキーにパラメータを取得
    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);

    // (todoの)idを引数にUserDao.todoFindById()を呼び出す
    TodoDao todoDao = new TodoDao();
    Todo todoDetail = todoDao.todoFindById(id);

    // キーtodoに対して上記の戻り値をリクエストスコープに対してセットする
    request.setAttribute("todo", todoDetail);

    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
    dispatcher.forward(request, response);


  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // 文字コードを設定
      request.setCharacterEncoding("UTF-8");

      // 記入され送信されたリクエストパラメータを取得
      int id = Integer.parseInt(request.getParameter("id"));
      String title = request.getParameter("title");
//      int userId = Integer.parseInt(request.getParameter("userId"));

      // idを引数にTODOを呼び出し、更新した値をリクエストスコープに対してセット
      TodoDao todoDao = new TodoDao();
      Todo todoDetail = todoDao.todoFindById(id);
      todoDetail.setTitle(title);

      
      boolean isEmpty = title.equals("");

      if (isEmpty) {
      // キーtodoに対して上記の戻り値をリクエストスコープに対してセットする
      request.setAttribute("todo", todoDetail);

      // リクエストスコープにエラーメッセージをセット→setAttribute
      request.setAttribute("errMsg", "空欄では登録できません");
      
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
      dispatcher.forward(request, response);
      return;
      }
      //リクエストスコープに格納されている値をDBに上書きする
      todoDao.update(id, title);
      
      

      response.sendRedirect("ListServlet");

    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
  }
}

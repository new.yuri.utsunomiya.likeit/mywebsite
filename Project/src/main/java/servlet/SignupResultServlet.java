package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.User;
import dao.UserDao;
import util.PasswordEncoder;

/**
 * Servlet implementation class SignupResultServlet
 */
@WebServlet("/SignupResultServlet")
public class SignupResultServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public SignupResultServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    response.getWriter().append("Served at: ").append(request.getContextPath());
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("utf-8");
    
    HttpSession session = request.getSession();

    try {
      // 記入され送信されたリクエストパラメータを取得
      String inputUserName = request.getParameter("userName");
      String inputEmail = request.getParameter("email");
      String inputPassword = PasswordEncoder.encodePassword(request.getParameter("password"));

      User user = new User();
      user.setName(inputUserName);
      user.setEmail(inputEmail);
      user.setPassword(inputPassword);
      
//    String encodedPassword = PasswordEncoder.encodePassword(inputPassword);


   // 登録が確定されたかどうか確認するための変数
      String confirmed = request.getParameter("confirm_button");

      switch (confirmed) {
      case "cancel":
          session.setAttribute("user", user);
          response.sendRedirect("SignupServlet");
          break;

      case "regist":
          UserDao.addUser(user);
          request.setAttribute("user", user);
          request.getRequestDispatcher("WEB-INF/jsp/signupResult.jsp").forward(request, response);
          break;
      }

      

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

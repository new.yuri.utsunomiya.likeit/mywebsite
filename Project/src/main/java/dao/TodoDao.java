package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import base.DBManager;
import beans.Todo;

public class TodoDao {

  public List<Todo> findAll(int user_id) {

    // DB接続
    Connection conn = null;
    List<Todo> todoList = new ArrayList<Todo>();

    try {
      conn = DBManager.getConnection();

      // SQL文の準備
      String sql = "SELECT * FROM todo WHERE user_id = ?";

      // SQL文の実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, user_id);
      ResultSet rs = pStmt.executeQuery();



      // レコードの内容をTodoインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        int userId = rs.getInt("user_id");

        Todo todo = new Todo(id, title, userId);
        todoList.add(todo);

      }
    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoList;

  }

  public static void addTodo(int id, String title, int userId) {
    Connection conn = null;
    PreparedStatement Pstmt = null;
    try {
      conn = DBManager.getConnection();
      String addSql = "INSERT INTO todo (id, title, user_id)VALUES(?,?,?)";

      Pstmt = conn.prepareStatement(addSql);
      Pstmt.setInt(1, id);
      Pstmt.setString(2, title);
      Pstmt.setInt(3, userId);
      Pstmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (Pstmt != null) {
          Pstmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }


  public Todo todoFindById(int id) {

    Connection conn = null;
    PreparedStatement Pstmt = null;
    try {
      conn = DBManager.getConnection();
      String Sql = " SELECT * FROM todo WHERE id = ?";

      Pstmt = conn.prepareStatement(Sql);
      Pstmt.setInt(1, id);
      ResultSet rs = Pstmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      id = rs.getInt("id");
      String title = rs.getString("title");
      int userId = rs.getInt("user_id");

      return new Todo(id, title, userId);


    } catch (SQLException e) {
      return null;
    } finally {
      try {
        if (Pstmt != null) {
          Pstmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        return null;
      }
    }
  }

  public void update(int id, String title) {
    Connection conn = null;
    PreparedStatement Pstmt = null;
    try {
      conn = DBManager.getConnection();
      String Sql = " UPDATE todo SET title = ? WHERE id = ? ";

      Pstmt = conn.prepareStatement(Sql);
      Pstmt.setString(1, title);
      Pstmt.setInt(2, id);
      Pstmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (Pstmt != null) {
          Pstmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public void delete(int id) {

    Connection conn = null;
    PreparedStatement Pstmt = null;
    try {
      conn = DBManager.getConnection();
      String deleteSql = "DELETE FROM todo WHERE id=?";
      Pstmt = conn.prepareStatement(deleteSql);
      Pstmt.setInt(1, id);
      Pstmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (Pstmt != null) {
          Pstmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public List<Todo> search(String title, int id) {
    // DB接続
    Connection conn = null;
    List<Todo> todoSearchList = new ArrayList<Todo>();

    try {
      conn = DBManager.getConnection();

      // SQL文の準備
      String sql = "SELECT * FROM todo WHERE title LIKE ? AND user_id = ? ";

      // SQL文の実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, "%" + title + "%");
      pStmt.setInt(2, id);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        int id1 = rs.getInt("id");
        String title1 = rs.getString("title");
        int userId = rs.getInt("user_id");

        Todo todo = new Todo(id1, title1, userId);
        todoSearchList.add(todo);
      }

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todoSearchList;
  }
  
  public void deleteUser(int id) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    
    try {
      conn = DBManager.getConnection();
      String deleteSql = "DELETE FROM todo WHERE user_id = ?";
      pStmt = conn.prepareStatement(deleteSql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();
      

      
      
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (pStmt != null) {
          pStmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        
        
      }
    }
  }
}

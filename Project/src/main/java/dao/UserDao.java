package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import beans.User;

public class UserDao {

  public static void addUser(User user) {
    Connection conn = null;
    PreparedStatement pStmt = null;

    try {
      conn = DBManager.getConnection();
      String insertSql = "INSERT INTO user (name, email, password)VALUES(?,?,?)";
      pStmt = conn.prepareStatement(insertSql);
      pStmt.setString(1, user.getName());
      pStmt.setString(2, user.getEmail());
      pStmt.setString(3, user.getPassword());
      pStmt.executeUpdate();

      System.out.println("inserting user has been completed");


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (pStmt != null) {
          pStmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();


      }
    }
  }

  public void deleteUser(String email) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    
    try {
      conn = DBManager.getConnection();
      String deleteSql = "DELETE FROM user WHERE email = ?";
      pStmt = conn.prepareStatement(deleteSql);
      pStmt.setString(1, email);
      pStmt.executeUpdate();
      
      System.out.println("deleting user has been completed");
      
      
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (pStmt != null) {
          pStmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
        
        
      }
    }
  }
  public User updateUser(String _name, String _email, String _password, int _id) {
    Connection conn = null;
    PreparedStatement pStmt = null;

    try {
      conn = DBManager.getConnection();
      String updateSql = "UPDATE user SET name = ?, email = ?, password = ? WHERE id = ? ";
      pStmt = conn.prepareStatement(updateSql);
      pStmt.setString(1, _name);
      pStmt.setString(2, _email);
      pStmt.setString(3, _password);
      pStmt.setInt(4, _id);
      pStmt.executeUpdate();

      System.out.println("updating user has been completed");


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (pStmt != null) {
          pStmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();


      }
    }
    return null;
    
  }



  // emailとpasswordでユーザー情報がテーブルにあるかをチェック
  public User findByLoginInfo(String _email, String _password) {

    // コネクションをゲットするためにコネクションconnを定義
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      // SELECT文の準備
      String sql = "SELECT * FROM user WHERE email = ? and password = ?";

      // SELECT文の実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, _email);
      pStmt.setString(2, _password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String name = rs.getString("name");
      String email = rs.getString("email");
      String password = rs.getString("password");

      return new User(id, name, email, password);


    } catch (SQLException e) {
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }



  public User findUserById(int _id) {

    // コネクションをゲットするためにコネクションconnを定義
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      // SELECT文の準備
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECT文の実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, _id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }
      int id = rs.getInt("id");
      String name = rs.getString("name");
      String email = rs.getString("email");
      String password = rs.getString("password");

      return new User(id, name, email, password);


    } catch (SQLException e) {
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}

<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TODO App | ユーザー登録</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesomeの読み込み-->
<script src="https://kit.fontawesome.com/f4ccc568fd.js"
	crossorigin="anonymous"></script>
</head>
<body class="bg-light">
	<!-- フォームが真ん中にレイアウトされるように、CSSに記載してある -->
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-5 mt-5">
				<h1 class="text-center">
					<i class="fa-solid fa-t"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-d"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-a"></i> <i class="fa-solid fa-p"></i> <i
						class="fa-solid fa-p"></i> / <i class="fa-solid fa-s"></i> <i
						class="fa-solid fa-i"></i> <i class="fa-solid fa-g"></i> <i
						class="fa-solid fa-n"></i> <i class="fa-solid fa-u"></i> <i
						class="fa-solid fa-p"></i>
				</h1>
			</div>
		</div>
		<div class="d-flex justify-content-center align-self-center">
			<div class="card-body">
				<form action="SignupResultServlet" method="post"
					style="width: 600px;">
					<div class="form-group row">
						<!-- 名前 -->
						<label for="inputUserName" class="col-3 col-form-label">名前</label>
						<div class="col-9">
							<input type="text" name="userName" id="inputUserName"
								class="form-control" value="${user.name}" readpnly>
						</div>
					</div>
					<div class="form-group row">
						<!-- メールアドレス -->
						<label for="inputEmail" class="col-3 col-form-label">メールアドレス</label>
						<div class="col-9">
							<input type="text" name="email" id="inputEmail"
								class="form-control" value="${user.email}" readpnly>
						</div>
					</div>

					<div class="form-group row">
						<!-- パスワード -->
						<label for="inputPassword" class="col-3 col-form-label">パスワード</label>
						<div class="col-9">
							<input type="password" name="password" id="inputPassword"
								class="form-control" value="${user.password}" readpnly>
						</div>
					</div>
					<div class="row">
						<div class="col s6 center-align">
							<button class="btn btn-lg btn-secondary btn-block" type="submit"
								name="confirm_button" value="cancel">修正</button>
						</div>
						<div class="col s6 center-align">
							<button class="btn btn-lg btn-primary btn-block" type="submit"
								name="confirm_button" value="regist">登録</button>
						</div>
					</div>

				</form>
			</div>
		</div>


	</div>
</body>
</html>
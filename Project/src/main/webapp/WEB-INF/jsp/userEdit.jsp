<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TODO App | ユーザー情報修正</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesomeの読み込み-->
<script src="https://kit.fontawesome.com/f4ccc568fd.js"
	crossorigin="anonymous"></script>
</head>
<body class="bg-light">

    <!-- header -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
            <div class="container">
                <a class="navbar-brand" href="ListServlet">
                    <!-- ナビゲーションバーの「TODOAPP」※ FontAwesome使いましたが深い意味ないです -->
                    <i class="fa-solid fa-t"></i>
                    <i class="fa-solid fa-o"></i>
                    <i class="fa-solid fa-d"></i>
                    <i class="fa-solid fa-o"></i>

                    <i class="fa-solid fa-a"></i>
                    <i class="fa-solid fa-p"></i>
                    <i class="fa-solid fa-p"></i>
                </a>

                <div class="d-flex">
                    <ul class="navbar-nav">
                        <li class="nav-item navbar-text" ><a href="UserEditServlet" style="text-decoration: none" >
                            ${userInfo.name}さん</a></li>
                        <li class="nav-item"><button type="button" class="btn btn-danger btn-sm"><a class="nav-link text-white"
							href="LogoutServlet">ログアウト</a></button></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <!-- /header -->
    
	<!-- フォームが真ん中にレイアウトされるように、CSSに記載してある -->
	<div class="container">
		<!-- <div class="row">
			<div class="col-6 offset-3 mb-5 mt-5">
				<h1 class="text-center">
					<i class="fa-solid fa-t"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-d"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-a"></i> <i class="fa-solid fa-p"></i> <i
						class="fa-solid fa-p"></i> / <i class="fa-solid fa-s"></i> <i
						class="fa-solid fa-i"></i> <i class="fa-solid fa-g"></i> <i
						class="fa-solid fa-n"></i> <i class="fa-solid fa-u"></i> <i
						class="fa-solid fa-p"></i>
				</h1>
			</div>
		</div> -->
		<div class="d-flex justify-content-center align-self-center">
			<div class="center-block">
				<div class="card-body">
					<form action="UserEditServlet" method="post" style="width: 600px;">
						<!-- エラーメッセージ start -->
						<c:if test="${errMsg != null}">
							<div class="alert alert-danger" role="alert">${errMsg}</div>
						</c:if>
						<!--エラーメッセージ  end  -->
						<div class="form-group row">
							<!-- ID -->
							<label for="id" class="col-3 col-form-label">ユーザーID</label>
							<div class="col-9">
								<input type="text" name="id" id="id" class="form-control"
									value="${userInfo.id}" readonly>
							</div>
						</div>
						<div class="form-group row">
							<!-- 名前 -->
							<label for="inputUserName" class="col-3 col-form-label">名前</label>
							<div class="col-9">
								<input type="text" name="userName" id="inputUserName"
									class="form-control" value="${userInfo.name}">
							</div>
						</div>
						<div class="form-group row">
							<!-- メールアドレス -->
							<label for="inputEmail" class="col-3 col-form-label">メールアドレス</label>
							<div class="col-9">
								<input type="text" name="email" id="inputEmail"
									class="form-control" value="${userInfo.email}">
							</div>
						</div>

						<div class="form-group row">
							<!-- パスワード -->
							<label for="inputPassword" class="col-3 col-form-label">パスワード</label>
							<div class="col-9">
								<input type="password" name="password" id="inputPassword"
									class="form-control" value="${userInfo.password}">
							</div>
						</div>
						<div class="row">
							<div class="col s6 center-align">
								<button class="btn btn-lg btn-primary btn-block" type="submit"
									name="confirm_button" value="edit">修正</button>
							</div>
						</div>

					</form>
					<div class="row" style="width: 630px">
						<div class="col s6 center-align">
							<button class="btn btn-lg btn-secondary btn-block"
								onclick="location.href='ListServlet'">戻る</button>
						</div>
					</div>
					<br>
					<div class="row" style="width: 630px">
						<div class="col s6 center-align">
							<button class="btn btn-sm btn-light btn-block"
								onclick="location.href='UserDeleteServlet'">退会する</button>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>
</body>
</html>
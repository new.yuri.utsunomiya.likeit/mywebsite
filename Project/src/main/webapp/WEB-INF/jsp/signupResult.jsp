<%@	page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>TODO App | ユーザー登録</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- FontAwesomeの読み込み-->
<script src="https://kit.fontawesome.com/f4ccc568fd.js"
	crossorigin="anonymous"></script>
</head>
<body class="bg-light">
	<!-- フォームが真ん中にレイアウトされるように、CSSに記載してある -->
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3 mb-5 mt-5">
				<h1 class="text-center">
					<i class="fa-solid fa-t"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-d"></i> <i class="fa-solid fa-o"></i> <i
						class="fa-solid fa-a"></i> <i class="fa-solid fa-p"></i> <i
						class="fa-solid fa-p"></i> / <i class="fa-solid fa-s"></i> <i
						class="fa-solid fa-i"></i> <i class="fa-solid fa-g"></i> <i
						class="fa-solid fa-n"></i> <i class="fa-solid fa-u"></i> <i
						class="fa-solid fa-p"></i>
				</h1>
			</div>
		</div>

		<div class="row">
			<div class="col-6 offset-3">

				<%-- <div class="row">
					<label for="loginId" class="col-3 font-weight-bold">ログインID</label>
					<div class="col-9">
						<p>${user.login_id }</p>
					</div>
				</div> --%>
				<%-- 	<div class="row">
					<label for="userName" class="col-3 font-weight-bold">名前</label>
					<div class="col-9">
						<p>${user.name }</p>
					</div>
				</div>

				<div class="row">
					<label for="email" class="col-3 font-weight-bold">メールアドレス</label>
					<div class="col-9">
						<p>${user.email}</p>
					</div>
				</div>

				<div class="row">
					<label for="createDate" class="col-3 font-weight-bold">パスワード</label>
					<div class="col-9">
						<p>${user.password}</p>
					</div>
				</div> --%>

				<div class="row">
					<div class="col s12">
						<p class="center-align">ユーザー登録が完了しました。</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12 center-align">
						<button class="btn btn-lg btn-primary btn-block" onclick="location.href='LoginServlet'">ログイン画面へ</button>
					</div>
				</div>
			</div>
		</div>


	</div>
</body>
</html>
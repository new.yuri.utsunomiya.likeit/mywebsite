CREATE DATABASE usermanagement DEFAULT CHARACTER SET UTF8;
USE usermanagement;


CREATE DATABASE mywebsite DEFAULT CHARACTER SET UTF8;
USE mywebsite;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

INSERT INTO `user` (`id`, `name`, `email`, `password`)
VALUES
  (1,'ユーザー1','urinurin11@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99');


CREATE TABLE `todo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `todo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3;

INSERT INTO todo (`id`, `title`, `user_id`)
VALUES
  (11,'SQL学習',1),
  (12,'Eclipse学習',1);